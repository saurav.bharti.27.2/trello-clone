import './App.css'
import {Route, Routes} from 'react-router-dom';
import Board from './components/Board';
import Header from './components/Header';
import Lists from './components/Lists';

function App() {

  return (
    <Routes>
      <Route path='/' element={<Header />}>
        <Route path='/boards' element={<Board />} />
        <Route path='/board/:id' element={< Lists />} />
        <Route />
      </Route>
    </Routes>
  )
}

export default App
