import CommonRequest from "./CommonRequest";
import { BASE_URL } from "./helper";

const API_KEY = import.meta.env.VITE_API_KEY
const TOKEN = import.meta.env.VITE_TOKEN 

export const getAllBoardsApi = async() => {
    return await CommonRequest("GET", `${BASE_URL}/members/me/boards?key=${API_KEY}&token=${TOKEN}`);
}

export const addBoardApi = async (board_name) => {
    return await CommonRequest("POST", `${BASE_URL}/boards/?name=${board_name}&key=${API_KEY}&token=${TOKEN}`)
}

export const deleteBoardApi = async (board_id) => {
    return await CommonRequest("DELETE", `${BASE_URL}/boards/${board_id}?key=${API_KEY}&token=${TOKEN}`)
}

export const updateBoardApi = async (id, name) => {
    return await CommonRequest("PUT", `${BASE_URL}/boards/${id}?key=${API_KEY}&token=${TOKEN}&name=${name}`)
}

export const fetchAllListsApi = async (list_id) => {
    return await CommonRequest("GET", `${BASE_URL}/boards/${list_id}/lists?key=${API_KEY}&token=${TOKEN}`)
}

export const fetchAllCardsApi = async (list_id) => {
    return await CommonRequest("GET", `${BASE_URL}/lists/${list_id}/cards?key=${API_KEY}&token=${TOKEN}`, {})
}

export const archiveListApi = async (list_id) => {
    return await CommonRequest("PUT", `${BASE_URL}/lists/${list_id}/closed?key=${API_KEY}&token=${TOKEN}&value=true`, {})
}

export const addListApi = async(inputListName, id) => {
    return await CommonRequest("POST", `${BASE_URL}/lists?name=${inputListName}&idBoard=${id}&key=${API_KEY}&token=${TOKEN}`, {})
}

export const updateListApi = async(name, id) => {
    return await CommonRequest("PUT", `${BASE_URL}/lists/${id}?key=${API_KEY}&token=${TOKEN}&name=${name}` , {})
}

export const addCardApi = async(list_id, cardName) => {
    return await CommonRequest("POST", `${BASE_URL}/cards?idList=${list_id}&key=${API_KEY}&token=${TOKEN}&name=${cardName}`, {})
}

export const fetchAllChecklistsApi = async(card_id) => {
    return await CommonRequest("GET", `${BASE_URL}/cards/${card_id}/checklists?key=${API_KEY}&token=${TOKEN}`, {})
}

export const fetchAllCheckItemsApi = async(checklist_id ) => {
    return await CommonRequest("GET", `${BASE_URL}/checklists/${checklist_id}/checkItems?key=${API_KEY}&token=${TOKEN}`, {})
}

export const addToChecklistApi = async(name, id) => {
    return await CommonRequest("POST", `${BASE_URL}/checklists?idCard=${id}&key=${API_KEY}&token=${TOKEN}&name=${name}`, {})
}

export const deleteChecklistApi = async(id) => {
    return await CommonRequest("DELETE", `${BASE_URL}/checklists/${id}?key=${API_KEY}&token=${TOKEN}`, {})
}

export const deleteCardApi = async(card_id) => {
    return await CommonRequest("DELETE", `${BASE_URL}/cards/${card_id}?key=${API_KEY}&token=${TOKEN}`, {})
}

export const addToCheckItemsApi = async(name, id) => {
    return await CommonRequest("POST", `${BASE_URL}/checklists/${id}/checkItems?name=${name}&key=${API_KEY}&token=${TOKEN}`, {})
}

export const deleteItemsFromChecklistApi = async(checklist_id, checkitem_id) => {
    return await CommonRequest('DELETE', `${BASE_URL}/checklists/${checklist_id}/checkItems/${checkitem_id}?key=${API_KEY}&token=${TOKEN}`, {})
}

export const changeTheChecklistStateApi = async(card_id, id, state) => {
    return await CommonRequest("PUT", `${BASE_URL}/cards/${card_id}/checkItem/${id}?key=${API_KEY}&token=${TOKEN}&state=${state}`, {})
}