import axios from "axios";

import React from 'react'

export default async function CommonRequest(methods, url, body, header) {

    let config = {
        method: methods,
        url,

        headers : header ? header: {
            'Content-Type' : 'application/json'
        },
        data : body 
    }

  return axios(config).then((response) => {
    return response;
  })
  .catch((err) => {
    console.log(err)
    throw new Error(err);
  })
}
