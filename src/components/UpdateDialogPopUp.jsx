import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import EditNoteIcon from '@mui/icons-material/EditNote';
import { useRef, useEffect,useState } from 'react';


export default function UpdateDialogPopUp({ providedFunction, type, label, id}) {
  const [open, setOpen] =  useState(false);
    

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  return (
    <React.Fragment>
      
      {
        type=="Update"
        &&
        <EditNoteIcon className='updateIcon' sx={{marginTop: '0.5rem', marginRight: '-1rem', '&:hover': {cursor: 'default'} }}  onClick={(e) => {e.stopPropagation(); handleClickOpen() } }> 
        </EditNoteIcon>
      }
      {
        type=="Add"
        &&
        <Button sx={{border: `${label=="Add an Item" ? '0.01rem solid black' : '0.1rem solid black' }`, margin: '0.5rem 0 0.8rem 0' }} onClick={handleClickOpen}  >
          {label}
        </Button>
      }

      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          component: 'form',
          onSubmit: (event) => {
            event.preventDefault();
            const formData = new FormData(event.currentTarget);
            const formJson = Object.fromEntries(formData.entries());
            const name = formJson.name;
            
            providedFunction(name, id)
            handleClose();
          },
        }}
      >
        
        <DialogTitle>{type}</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            required
            margin="dense"
            id="name"
            name="name"
            label={label}
            type="name"
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}  className='cancelBtn'>Cancel</Button>
          <Button type="submit" className='submitBtn'>{type}</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}