import * as React from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import LibraryAddCheckIcon from '@mui/icons-material/LibraryAddCheck';
import { ToastContainer, toast } from "react-toastify";
import {Box, LinearProgress, Paper, TextField} from '@mui/material'
import UpdateDialogPopUp from './UpdateDialogPopUp';
import LinearWithValueLabel from './LinearWithValueLabel';

import { useEffect, useState } from 'react';
import axios from 'axios';
import CheckItemsComponent from './CheckItemsComponent';
import { addToChecklistApi, deleteCardApi, deleteChecklistApi, fetchAllCheckItemsApi, fetchAllChecklistsApi } from '../services/api';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));


export default function DialogForCard({card, list, setCards, setLists}) {

  const API_KEY = import.meta.env.VITE_API_KEY
  const TOKEN = import.meta.env.VITE_TOKEN

  const [open, setOpen] = useState(false);

  const [checklists, setChecklists] = useState()
  const [checkItems, setCheckItems] = useState({})



  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const fetchAllChecklists= async ()=> {

    try{
      let response = await fetchAllChecklistsApi(card.id)

      response.data.map((checklist) => {
        fetchAllCheckItems(checklist.id)
      })
      setChecklists(response.data)
    }
    catch(err){
      toast.error('Unable to fetch the checklists')
      console.log(err)
    }

  }

  const fetchAllCheckItems= async(checklist_id)=> {

      try{
        let response = await fetchAllCheckItemsApi( checklist_id)

        setCheckItems(prevState => ({
          ...prevState,
          [checklist_id] : response.data
        }))

      }catch(err){
        toast.error("Unable to fetch check-items.")
        console.log(err)
      }
  }

  const addToChecklist = async(name, id)=> {
    
    try{
      let response = await addToChecklistApi(name, id)
      setChecklists(prevState=> ([
        ...prevState,
        response.data
      ]))
      setCheckItems(prevState => ({
        ...prevState,
        [response.data.id]: []
      }))

      toast.success('Successfully added checklist to card')

    }catch(err){
      toast.error('Unable to add checklist')
      console.log(err);
    }


  }

  const deleteChecklist = async(id)=> {

    try{
      let response = await deleteChecklistApi(id)

      setChecklists(prevState => {
        let filterChecklist = prevState.filter((checklist)=> {
          if(checklist.id != id){
            return checklist
          }
        })
        return filterChecklist
      })

      setCheckItems(prevState => {
        
        let filteredItem= {}
        Object.entries(prevState).forEach((checkitem) => {
          if(checkitem[0] != id){
            filteredItem[checkitem[0]] = checkitem[1]
          }
         })
         
        return filteredItem
      })

      toast.success('Successfully deleted the checklist')

    }catch(err){
      toast.error('Unable to delete the checkllist.')
      console.log(err)
    }
  }

  const deleteCard = async() => {

    try{
      let response = await deleteCardApi(card.id)
      // setCards(prevState => ({
      //   ...prevState,
      //   [list.id]: [...prevState[list.id].filter]
      // }))
      setCards(prevState => {
        let filteredCards = {}
        
        Object.entries(prevState).map((eachList) => {
          filteredCards[eachList[0]] = []
          eachList[1] && eachList[1].map(each_card => {
            if(each_card.id != card.id){
              filteredCards[eachList[0]].push(each_card)
            }
          })
        })

        return filteredCards
      })

    toast.success('Successfully deleted card')
    handleClose()

    }
    catch(err){
      toast.error('Unable to delete the card.')
      console.log(err)
    }

  }





  useEffect(() => {
    fetchAllChecklists()
  }, [] )


  return (
    <React.Fragment>

      <Button variant="outlined" onClick={handleClickOpen} sx={{width: '100%', border: 'none', '&:hover': {border: 'none'} }}>
        {card.name} 
      </Button>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}

      >
        <DialogTitle sx={{ m: 0, p: 2, minWidth: '600px' }} id="customized-dialog-title">
          <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
            <Typography variant='h8'>
              {card.name}
            </Typography>
            <DeleteIcon onClick={()=> {deleteCard()}} sx={{marginRight: '3rem', marginTop:'-0.5rem', '&:hover': {cursor: 'pointer'}}}  />
          </Box>
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent dividers>
          <Typography gutterBottom >
            
            
            <UpdateDialogPopUp providedFunction={addToChecklist} type="Add" label="Add Checklist to Card" id={card.id} />
            

             
          </Typography>

          <Box>
            <Box>
              {
                checklists && checklists.map((checklist) => {
                  return <Box className="checklist">
                        <Box className="checklist_nav" sx={{display: 'flex', justifyContent: 'space-between'}}>
                          <LibraryAddCheckIcon />
                          <Typography sx={{marginRight: 'auto', paddingLeft: '0.4rem'}} > {checklist.name.toUpperCase()} </Typography>
                          <Button sx={{backgroundColor:'lightblue', padding: '0.1rem 0' }} onClick={()=> {deleteChecklist(checklist.id)}} >Delete</Button>
                        </Box>
                        
                        <CheckItemsComponent card_id={card.id} checklist_id={checklist.id} checkItems={checkItems} setCheckItems={setCheckItems} />
                    </Box>
                })
              }
            </Box>
          </Box>

        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Save changes
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </React.Fragment>
  );
}