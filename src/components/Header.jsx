import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Outlet } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

export default function Header() {

  const navigate = useNavigate()

  function navigateToBoards(){
    navigate('/boards')
  }
  return (
    <Box sx={{ flexGrow: 1}}>
      <AppBar position="static" sx={{ backgroundColor: 'black'}}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 , color: 'white'}}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Trello
          </Typography>

          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <Box sx={{width: '8rem', padding: '0 1.8rem 0 1rem', '&:hover' : {cursor: 'pointer'}}} onClick={navigateToBoards}>
              Boards

            </Box>
          </Typography>
        </Toolbar>
      </AppBar>

      <Outlet />
    </Box>
  );
}