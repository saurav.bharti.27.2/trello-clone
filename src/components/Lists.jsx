import React, { Fragment, useEffect, useRef, useState } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import CircularProgress from '@mui/material/CircularProgress';
import {Box, Paper, Container, Typography, TextField, Button} from '@mui/material'
import { useGlobalContext } from '../context/GlobalContext';
import ClearIcon from '@mui/icons-material/Clear';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import UpdateDialogPopUp from './UpdateDialogPopUp.jsx'
import DialogForCard from './DialogForCard.jsx';
import { ToastContainer, toast } from "react-toastify";
import { addCardApi, addListApi, archiveListApi, fetchAllCardsApi, fetchAllListsApi, updateListApi } from '../services/api.jsx';
import IndividualListComponent from './IndividualListComponent.jsx';

export default function Lists() {

    const {id} = useParams()
    const {loading, setLoading} = useGlobalContext()

    const [inputChecker, setInputChecker] = useState(false)

    const [lists, setLists] = useState([])

    const color= ['lightgreen', 'lightblue', 'orange', 'green', '#f74fb1']

    const fetchAllLists= async ()=> {

        try{
            setLoading(true)

            let response = await fetchAllListsApi(id)

            setLists(response.data)

        }catch(err){
            toast.error("Error while fetching the list")
            console.log(err, "error while fetching list for board_id", id) 
            setLoading(false)
        }
    }



    async function archiveList(list_id){

        try{
            let response = await archiveListApi(list_id)

            setLists( prevState => {
                let filteredList = prevState.filter((list) =>{
                    return list.id !== list_id
                })

                return filteredList

            })
            toast.success("Successfully deleted the list.")

        }catch(err){
            console.log(err)
            toast.error("Unable to delete the list")
        }
    }


    const addListHandler =()=> {
        setInputChecker(true)
    }



    const addList = async(event)=> {
        event.preventDefault()

        let inputListName = event.target.elements['standard-basic'].value
        event.target.elements['standard-basic'].value =''

        try{

            let response = await addListApi(inputListName, id)

            setLists(prevState => [
                response.data,
                ...prevState
            ])

            toast.success("List successfully added")

        }catch(err){
            console.log(err)
            toast.error("Unable to delete the list")
        }


    }

    const updateList= async(name, id)=> {
    
        try{
            let response = await updateListApi(name, id)

            setLists(prevState => {
                let newList = prevState.map((list) => {
                    if(list.id==id){
                        list.name = name
                    }
                    return list
                })
                return newList
            })

            toast.success('List updated successfully.')
        }
        catch(err){
            console.log(err)

            toast.error("Unable to update the list")
        }

    }


    useEffect(()=> {
        fetchAllLists()
    }, [])

  return (
    <Box 
        sx={{
        backgroundImage: `url(https://trello-backgrounds.s3.amazonaws.com/SharedBackground/1280x1920/b702c885ab25db5b537bec881750ca08/photo-1709983967470-f6c72b61cc62.jpg)`,
        backgroundSize: 'cover',
        }}
     >
        <ToastContainer />
        {
            loading && 
            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <CircularProgress />
            </Box>
        }
        {

            <Box sx={{width: '100%', overflowX: 'auto', minHeight: '95vh', 
            }}>

                <Box sx={{display:'flex', flexDirection: 'row-reverse' , justifyContent:'flex-end', margin:'0', }}>
                    
                    <Paper elevation={15} sx={{width: '12rem', height: '10rem', border: '2px solid black', backgroundColor: '#edf2ef', display:'flex', justifyContent: 'center', alignItems: 'center', margin: '0.5rem 0.2rem' }} > 
                        {
                            !inputChecker && <Box  sx={{ width: '12rem', display:'flex', flexDirection: 'column', alignItems:'center'}} onClick={addListHandler}>
                                <AddCircleOutlineIcon />
                                Add List 
        
                            </Box>
                        }
                        {
                            inputChecker && <form onSubmit={(event) => addList(event)}>
                                <Box sx={{ width: '12rem', height: '50%', margin: '0.5rem auto'}}>

                                    <TextField id="standard-basic" label="List Name" variant="standard" />
                                    <Button variant="contained" sx={{width:'50%', height: '35%', marginTop: '0.6rem'}} type='submit' >Save</Button>
                                </Box>
                            </form>
                        }
                    </Paper>
                    {
                        lists && Object.entries(lists).map((list, index)=> {
                            
                            return (
                                <Box sx={{margin: '0 1.2rem'}}>
                                    <Paper className={`${list[1].id}`}    elevation={15} sx={{width: '12rem', height: '10rem', border: '2px solid black', backgroundColor: color[index%5], margin: '0.5rem 0.2rem', display:'flex', flexDirection: 'column' , justifyContent: 'flex-start', alignItems: 'center', textAlign:'auto', lineBreak:'anywhere',  '&:hover': {cursor: 'pointer'} }}>
                                        <Container sx={{display: 'flex', justifyContent: 'space-between', height: '45%'}}>
                                            <UpdateDialogPopUp  providedFunction={updateList} type="Update" label="Update List's name" id={list[1].id} />
                                            <ClearIcon onClick={()=> {archiveList(list[1].id)}}  className='clearIcon' sx={{marginTop: '0.5rem', marginRight: '-1rem', '&:hover': {cursor: 'default'} }} />
                
                                        </Container>
                                        <Typography sx={{overflowY: 'auto'}}>  {list[1].name} </Typography> 
                                    </Paper>

                                    <IndividualListComponent list={list[1]} setLists={setLists} />
    
    
                                </Box>
    
                            )
                        })
                    }
            </Box>


                
            </Box>

        }

    </Box>
  )
}
