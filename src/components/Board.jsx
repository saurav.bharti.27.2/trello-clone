import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import { Fragment } from "react";
import { Box, Container } from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import ClearIcon from "@mui/icons-material/Clear";
import { TextField, Button, Paper, Typography } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import EditNoteIcon from "@mui/icons-material/EditNote";
import UpdateDialogPopUp from "./UpdateDialogPopUp.jsx";
import { useNavigate } from "react-router-dom";
import { useGlobalContext } from "../context/GlobalContext.jsx";
import { ToastContainer, toast } from "react-toastify";
import {
  addBoardApi,
  deleteBoardApi,
  updateBoardApi,
} from "../services/api.jsx";

export default function Board() {

  const navigate = useNavigate();

  const { loading, setLoading, board, setBoard, getAllBoards } =
    useGlobalContext();

  const [newBoardInput, setNewBoardInput] = useState(false);

  const boardName = useRef();

  async function addBoard(event) {
    event.preventDefault();

    try {
      let response = await addBoardApi(boardName.current.value);

      setBoard((prevState) => [...prevState, response.data]);
      toast.success(`Successfully added Board`);
    } catch (err) {
      console.log(err);
      toast.error(`Error while adding Board`);
    }
  }

  async function deleteBoard(board_id) {
    try {
      let response = await deleteBoardApi(board_id);

      setBoard((prevState) => {
        let filteredBoard = prevState.filter((eachBoard) => {
          return eachBoard.id !== board_id;
        });
        return filteredBoard;
      });

      toast.success("Board successfully deleted");
    } catch (err) {
      toast.error("Error while deleting the Board.");
      console.log(err);
    }
  }

  const updateBoard = async ( name, id) => {
    // event.
    try {
      let response = await updateBoardApi(id, name);
      
      setBoard((prevState) => {
        let newBoard = prevState.map((curBoard) => {
          if (curBoard.id == id) {
            curBoard.name = name;
          }

          return curBoard;
        });
        return newBoard;
      });

      toast.success("Board successfully updated");
    } catch (err) {
      console.log(err);
      toast.error("Error: Unable to update the board name");
    }
  };

  function handleABoard(board_id, event) {
    navigate(`/board/${board_id}`);
  }

  const handlePaperClick = (event, board_id) => {
    if (
      event.target.closest(".board")
    ) {
      handleABoard(board_id, event);
    }
  };

  useEffect(() => {
    getAllBoards();
  }, []);

  const addBoardHandler = () => {
    setNewBoardInput(true);
  };

  const color = ["lightgreen", "lightblue", "orange", "lightblue", "#f2aed6"];

  return (
    <Fragment>
      <ToastContainer />
      {loading && (
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <CircularProgress />
        </Box>
      )}
      {!loading && (
        <Box
          sx={{
            display: "grid",
            gridTemplateColumns: "repeat(5, 1fr)",
            marginTop: "2rem",
          }}
        >
          <Paper
            elevation={15}
            sx={{
              width: "12rem",
              height: "10rem",
              border: "2px solid black",
              backgroundColor: "#edf2ef",
              margin: "1.5rem 0.5rem 1.5rem 0.5rem",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              margin: "0.5rem auto",
            }}
          >
            {!newBoardInput && (
              <Box
                sx={{ display: "flex", alignItems: "center" }}
                onClick={addBoardHandler}
              >
                <AddCircleOutlineIcon />
                Add Board
              </Box>
            )}
            {newBoardInput && (
              <form onSubmit={(event) => addBoard(event)}>
                <Box
                  sx={{ width: "50%", height: "50%", margin: "0.5rem auto" }}
                >
                  <TextField
                    inputRef={boardName}
                    name="boardNameText"
                    id="standard-basic"
                    label="Board Name"
                    variant="standard"
                    required
                  />
                  <Button
                    variant="contained"
                    sx={{ width: "50%", height: "35%", marginTop: "0.6rem" }}
                    type="submit"
                  >
                    Save
                  </Button>
                </Box>
              </form>
            )}
          </Paper>
          {board &&
            board.map((board, index) => {
              return (
                <Paper
                  className="board"
                  onClick={(event) => handlePaperClick(event, board.id)}
                  elevation={15}
                  sx={{
                    width: "12rem",
                    height: "10rem",
                    border: "2px solid black",
                    backgroundColor: color[index % 5],
                    margin: "0.5rem auto",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    textAlign: "auto",
                    lineBreak: "anywhere",
                    "&:hover": { cursor: "pointer" },
                  }}
                >
                  <Container
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      height: "45%",
                    }}
                  >
                    <UpdateDialogPopUp
                      providedFunction={updateBoard}
                      type="Update"
                      label="Update the board"
                      id={board.id}
                    />
                    <ClearIcon
                      className="clearIcon"
                      sx={{
                        marginTop: "0.5rem",
                        marginRight: "-1rem",
                        "&:hover": { cursor: "default" },
                      }}
                      onClick={(e) => {
                        e.stopPropagation();
                        deleteBoard(board.id);
                      }}
                    />
                  </Container>
                  <Typography sx={{ overflowY: "auto" }}>
                    {" "}
                    {board.name}{" "}
                  </Typography>
                </Paper>
              );
            })}
        </Box>
      )}
    </Fragment>
  );
}
