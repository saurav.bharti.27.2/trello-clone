import React, { useEffect, useState } from 'react'
 import CircularProgress from '@mui/material/CircularProgress';
 import {Box, Paper, Container, Typography, TextField, Button} from '@mui/material'
 import { useGlobalContext } from '../context/GlobalContext';
 import ClearIcon from '@mui/icons-material/Clear';
 import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
 import UpdateDialogPopUp from './UpdateDialogPopUp.jsx'
 import DialogForCard from './DialogForCard.jsx';
 import { ToastContainer, toast } from "react-toastify";
 import { addCardApi, fetchAllCardsApi } from '../services/api.jsx';

export default function IndividualListComponent({list, setLists}) {

    const [cards, setCards] = useState({})
    const [newCardInput, setNewCardInput] = useState({})
    const {loading, setLoading} = useGlobalContext()

    const fetchAllCards = async()=> {
        
        try{

            let response = await fetchAllCardsApi(list.id)
            setCards(prev => ({
                ...prev,
                [list.id] : response.data
            }))
            
            setLoading(false)
        }catch(err){
            console.log(err, "error while fetching cards for list_id", list.id)
            toast.error("Error while fetching the cards.")
            setLoading(false)
        }
        
    }

    const addCardHandler = ()=> {
        
        setNewCardInput(prev => ({
            ...prev,
            [list.id] : !prev[list.id]
        }));
    }

    const addCard = async (event, list_id)=> {
        event.preventDefault()

        let cardName = event.target.elements['standard-basic'].value;
        event.target.elements['standard-basic'].value= ''

        try{

            let response = await addCardApi(list_id, cardName)

            setCards(prevState => {
                return ({
                    ...prevState,
                    [list_id] : [ ...prevState[list_id], response.data]
                })
            })

            toast.success('Successfully added a card')

        }catch(err){
            toast.error("Error while add a card with list_id")
            console.log(err)
            
        }

    }

    useEffect(() => {
        fetchAllCards()
    }, [])


  return (
    <>
        <Box>
            <Box sx={{maxHeight: '55vh', overflowY: 'auto', overflowX: 'hidden'}}>
                {
                    cards[list.id] && cards[list.id].map((card)=> {
                        return (
                            <Paper sx={{height: '2.5rem', margin: '0.4rem', padding: '0 0.4rem', width: '12rem', display: 'flex', alignItems: 'center'}}>
                                <DialogForCard card={card} list = {list} setCards={setCards} setLists={setLists}/>
                            </Paper>
                        )
                    })
                }
            </Box>

            {

                !newCardInput[list.id] && 
                <Paper elevation={15} sx={{height: '2.5rem', margin: '0.4rem', width: '12rem', display: 'flex', alignItems: 'center', '&:hover' : {cursor : 'pointer'}}} >
                    <Box onClick={() => addCardHandler(list.id)} sx={{width:'100%', display: 'flex', justifyContent:'space-evenly', alignItems: 'center' }} >
                        <Typography> Add a card </Typography> 
                        <AddCircleOutlineIcon />
                    </Box>
                </Paper>

            }
            {
                newCardInput[list.id]  && 
                    
                <form onSubmit={(event)=> {addCard(event, list.id)}}>
                    <Paper sx={{height: '4rem', margin: '0.4rem', width: '12rem', display: 'flex', alignItems: 'center', padding: '0 0.4rem'}}>
                        <TextField id="standard-basic" label="Card Name" variant="standard" />
                        <Button id={`btn${list.id}`} type='submit' variant="contained" sx={{width:'30%', margin: '0 0.2rem', height: '35%', marginTop: '0.6rem'}} >Save</Button>
                    </Paper>
                </form>

            }

        </Box>
    </>
  )
}
