import React, { Fragment, useState } from "react";
import LinearWithValueLabel from "./LinearWithValueLabel";
import UpdateDialogPopUp from "./UpdateDialogPopUp";
import { Typography, Box } from "@mui/material";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { ToastContainer, toast } from "react-toastify";

import axios from "axios";
import {
  addToCheckItemsApi,
  changeTheChecklistStateApi,
  deleteItemsFromChecklistApi,
} from "../services/api";

export default function CheckItemsComponent({
  card_id,
  checklist_id,
  checkItems,
  setCheckItems,
}) {
  let checkedItem = 0;
  checkItems[checklist_id] &&
    checkItems[checklist_id].map((item) => {
      if (item.state == "complete") {
        checkedItem += 1;
      }
    });

  const addToCheckItems = async (name, id) => {
    try {
      let response = await addToCheckItemsApi(name, id);
      setCheckItems((prevState) => ({
        ...prevState,
        [checklist_id]: [...prevState[checklist_id], response.data],
      }));
      toast.success("Item added to checklist");
    } catch (err) {
      toast.error("Unable to add item in checklist");
      console.log(err);
    }
  };

  const deleteItemsFromChecklist = async (checkitem_id) => {
    try {
      let response = await deleteItemsFromChecklistApi(
        checklist_id,
        checkitem_id
      );

      setCheckItems((prevState) => {
        let filteredItem = {};
        Object.entries(prevState).map((checkitems) => {
          filteredItem[checkitems[0]] = [];

          checkitems[1].map((checkitem) => {
            if (checkitem.id != checkitem_id) {
              filteredItem[checkitems[0]].push(checkitem);
            }
          });
        });

        return filteredItem;
      });

      toast.success("Item successfully deleted from checklist");
    } catch (err) {
      toast.error("Unable to delete item from checklist");
      console.log(err);
    }
  };

  let totalCheckItems = checkItems[checklist_id]
    ? checkItems[checklist_id].length
    : 0;

  let checkedPercentage =
    checkedItem == 0
      ? 0
      : Math.round(
          ((Number(checkedItem) / Number(totalCheckItems)) * 100).toFixed(2)
        );

  const changeTheChecklistState = async (id, state) => {
    try {
      if (state == "complete") {
        state = "incomplete";
      } else {
        state = "complete";
      }

      setCheckItems((prevState) => {
        let filteredItem = {};

        Object.entries(prevState).forEach((checkitems) => {
          let tempObj = [];

          checkitems[1].forEach((checkitem) => {
            let tempCheckItem = {};

            if (checkitem.id == id) {
              tempCheckItem = { ...checkitem, state: state };
            } else tempCheckItem = { ...checkitem };

            tempObj.push(tempCheckItem);
          });

          filteredItem[checkitems[0]] = tempObj;
        });
        return filteredItem;
      });

      checkedItem += state == "complete" ? 1 : 0;

      let response = await changeTheChecklistStateApi(card_id, id, state);
    } catch (err) {
      toast.err("Unable to change the state of item");
      console.log(err);
    }
  };

  return (
    <Fragment>
      <LinearWithValueLabel value={checkedPercentage} />

      <UpdateDialogPopUp
        providedFunction={addToCheckItems}
        type="Add"
        label="Add an Item"
        id={checklist_id}
      />

      <Box sx={{ marginLeft: "1rem" }}>
        {checkItems[checklist_id] &&
          checkItems[checklist_id].map((checkItem) => {
            return (
              <FormGroup
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={checkItem.state === "complete" ? true : false}
                      onClick={() => {
                        changeTheChecklistState(checkItem.id, checkItem.state);
                      }}
                    />
                  }
                  label={checkItem.name}
                />
                <DeleteForeverIcon
                  onClick={() => {
                    deleteItemsFromChecklist(checkItem.id);
                  }}
                />
              </FormGroup>
            );
          })}
      </Box>
    </Fragment>
  );
}
