import React from "react";
import { useContext, useState } from "react";
import { getAllBoardsApi } from "../services/api";

const GlobalContext = React.createContext()
const API_KEY = import.meta.env.VITE_API_KEY
const TOKEN = import.meta.env.VITE_TOKEN

export function useGlobalContext(){
    return useContext(GlobalContext)
}



export function GlobalProvider({children}){
    const [loading, setLoading] = useState(true)
    const [board, setBoard] = useState([]);

    async function getAllBoards(){
        
        try{
            let response = await getAllBoardsApi()

            setBoard(response.data)
            setLoading(false)

        }catch(err) {
            console.log(err)
            return err
        }
    }

    const value={
        loading,
        setLoading,
        getAllBoards,
        board,
        setBoard
    }

    return <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
}